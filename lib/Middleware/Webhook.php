<?php
/**
 * Configuration
 * PHP version 5
 *
 * @category Class
 * @package  AstroPayments
 * @author   CMcEwen
 */

/**
	* Mock
	*
	* Mock handler for API calls
	*
	* Contact: integrations@goastro.com
	*/

namespace AstroPayments\Middleware;;

class Webhook
{
	static function verify($json_payload,$signature,$signature_key){
		$expected_signature=hash_hmac('sha256',$json_payload,$signature_key);
		if($expected_signature==$signature) return true;
		else return false;
	}
}