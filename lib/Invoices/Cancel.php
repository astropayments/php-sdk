<?php
namespace AstroPayments\Invoices;
use \AstroPayments\API as API;
use \AstroPayments\Exception\CurlException as CurlException;
use \AstroPayments\Exception\SDKException as SDKException;
use \AstroPayments\Exception\ueException as ueException;


class Cancel{

	public static function post($Data=array()){
		if(!array_key_exists("invoice_key",$Data)) throw new SDKexception("Cancel post requires invoice_key");

		$invoice_key=$Data["invoice_key"];
		unset($Data["invoice_key"]);

		$Response_type="json";
		$Path="/invoices/$invoice_key/cancel";
		$Params=[];

		try{
			return API::runCall('post',$Path,$Data,$Params,$Response_type);
		}
		catch(CurlException $e){
			throw $e;
		}
		catch(SDKException $e){
			throw $e;
		}
		catch(ueException $e){
			throw $e;
		}
		catch(\Exception $e){
			throw new SDKException("Unexpected exception thrown");
		}
	}
}
?>