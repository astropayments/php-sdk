<?php
namespace AstroPayments\Paymentengine;
use \AstroPayments\API as API;
use \AstroPayments\Exception\CurlException as CurlException;
use \AstroPayments\Exception\SDKException as SDKException;
use \AstroPayments\Exception\ueException as ueException;


class Payrequests{

	public static function post($Data=array()){
		$Response_type="json";
		$Path="/paymentengine/payrequests";
		$Params=[];

		try{
			return API::runCall('post',$Path,$Data,$Params,$Response_type);
		}
		catch(CurlException $e){
			throw $e;
		}
		catch(SDKException $e){
			throw $e;
		}
		catch(ueException $e){
			throw $e;
		}
		catch(\Exception $e){
			throw new SDKException("Unexpected exception thrown");
		}
	}

	public static function get($Data=array()){
		$Response_type="json";
		$Path="/paymentengine/payrequests";
		$Params=[];

		if(array_key_exists("requestkey",$Data)){
			$Path.='/'.$Data["requestkey"];
			unset($Data["requestkey"]);
		}

		try{
			return API::runCall('get',$Path,$Data,$Params,$Response_type);
		}
		catch(CurlException $e){
			throw $e;
		}
		catch(SDKException $e){
			throw $e;
		}
		catch(ueException $e){
			throw $e;
		}
		catch(\Exception $e){
			throw new SDKException("Unexpected exception thrown");
		}
	}

	public static function delete($Data=array()){
			if(!array_key_exists("requestkey",$Data)) throw new SDKexception("Payrequests delete requires requestkey");

		$Response_type="json";
		$Path="/paymentengine/payrequests";
		$Params=[];

		if(array_key_exists("requestkey",$Data)){
			$Path.='/'.$Data["requestkey"];
			unset($Data["requestkey"]);
		}

		try{
			return API::runCall('delete',$Path,$Data,$Params,$Response_type);
		}
		catch(CurlException $e){
			throw $e;
		}
		catch(SDKException $e){
			throw $e;
		}
		catch(ueException $e){
			throw $e;
		}
		catch(\Exception $e){
			throw new SDKException("Unexpected exception thrown");
		}
	}
}
?>